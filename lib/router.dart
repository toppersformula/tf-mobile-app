import 'dart:math' as math;
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:toppers_formula/pages/root.page.dart';
import 'config/app_config.dart';
import 'pages/auth/auth.page.dart';
import 'pages/home/home.page.dart';
import 'package:toppers_formula/widgets/shared/not_found.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'pages/login/login_page.dart';
import 'pages/login/reset_password.dart';
import 'pages/registration/register_page.dart';
import 'pages/registration/verify_otp.dart';


class Routes {
  static String auth = "/auth";
  static String home = "/home";
  static String login = "/login";
  static String register = "/register";
  static String verification = "/verification";
  static String resetPassword = "/resetPassword";
  static String rootPage = "/root";
  static String termsOfUse = "/termsOfUse";
  static String privacyPolicy = "/privacyPolicy";

  static void configureRoutes(Router router) {
    router.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print("ROUTE WAS NOT FOUND !!!");
      return NotFoundPage(
        appTitle: "Coming Soon",
        icon: FontAwesomeIcons.solidSmile,
        title: "Coming Soon",
        message: "Under Development",
        iconColor: Colors.green,
      );
    });

    Handler authHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return AuthPage();
    });

    Handler homeHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return HomePage();
    });

    Handler rootHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return RootPage();
        });

    Handler registerHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return RegistrationPage();
        });

    Handler loginHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return LoginPage();
        });

    Handler verificationHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return OTPVerificationPage();
        });

    Handler resetPasswordHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          return ResetPasswordPage();
        });

    Handler termsAndCondHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print(TERMS_AND_CONDITION_URL);
      return WebviewScaffold(
        url: TERMS_AND_CONDITION_URL,
        appBar: AppBar(
          title: Text("Terms & Conditions"),
        ),
      );
    });

    Handler privacyPolicyHandler = Handler(handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print(PRIVACY_POLICY_URL);

      return WebviewScaffold(
        url: PRIVACY_POLICY_URL,
        appBar: AppBar(
          title: Text("Privacy Policy"),
        ),
      );
    });

    router.define(auth, handler: authHandler);
    router.define(home, handler: homeHandler);
    router.define(register, handler: registerHandler);
    router.define(login, handler: loginHandler);
    router.define(verification, handler: verificationHandler);
    router.define(resetPassword, handler: resetPasswordHandler);
    router.define(rootPage, handler: rootHandler);
    router.define(termsOfUse, handler: termsAndCondHandler);
    router.define(privacyPolicy, handler: privacyPolicyHandler);

  }
}

class AppRouter {
  static Router router;
}

class LastBytePageRoute<T> extends PageRoute<T> {
  LastBytePageRoute({
    @required this.builder,
    RouteSettings settings,
    bool fullscreenDialog: true,
  }) : super(settings: settings, fullscreenDialog: fullscreenDialog);

  final WidgetBuilder builder;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 300);

  @override
  bool get maintainState => true;

  @override
  Color get barrierColor => null;

  @override
  String get barrierLabel => null;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return builder(context);
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return new LastBytePageTransition(routeAnimation: animation, child: child);
  }
}

class LastBytePageTransition extends StatelessWidget {
  LastBytePageTransition(
      {Key key, @required this.routeAnimation, @required this.child})
      : super(key: key);

  final Animation<double> routeAnimation;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new ClipOval(
      clipper: new CircleRevealClipper(routeAnimation.value),
      child: new SlideTransition(
          position: new Tween<Offset>(
                  begin: const Offset(0.0, 0.25), end: Offset.zero)
              .animate(routeAnimation),
          child: child),
    );
  }
}

class CircleRevealClipper extends CustomClipper<Rect> {
  CircleRevealClipper(this.revealPercent);

  final double revealPercent;

  @override
  Rect getClip(Size size) {
    final epicenter = new Offset(size.width / 0.9, size.height / 0.9);

    double theta = math.atan(epicenter.dy / epicenter.dx);
    final distanceToCorner = epicenter.dy / math.sin(theta);

    final radius = distanceToCorner * revealPercent;
    final diameter = 2 * radius;

    return new Rect.fromLTWH(
        epicenter.dx - radius, epicenter.dy - radius, diameter, diameter);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return true;
  }
}

// Slide right
class SlideRightRoute extends PageRouteBuilder {
  final Widget widget;

  SlideRightRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new SlideTransition(
            position: new Tween<Offset>(
              begin: const Offset(-1.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          );
        });
}

// Slide right
class SlideLeftRoute extends PageRouteBuilder {
  final Widget widget;

  SlideLeftRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new SlideTransition(
            position: new Tween<Offset>(
              begin: const Offset(1.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          );
        });
}

// FADE Like Uber
class ScaleRoute extends PageRouteBuilder {
  final Widget widget;

  ScaleRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new ScaleTransition(
            scale: new Tween<double>(
              begin: 0.0,
              end: 1.0,
            ).animate(
              CurvedAnimation(
                parent: animation,
                curve: Interval(
                  0.00,
                  0.50,
                  curve: Curves.linear,
                ),
              ),
            ),
            child: ScaleTransition(
              scale: Tween<double>(
                begin: 1.5,
                end: 1.0,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Interval(
                    0.50,
                    1.00,
                    curve: Curves.linear,
                  ),
                ),
              ),
              child: child,
            ),
          );
        });
}
