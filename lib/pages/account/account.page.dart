import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/authentication/authentication.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';

class AccountPage extends StatefulWidget {
  @override
  _AccountPageState createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TFAppBar(title: "Account", center: true,),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ListTile(
            title: Text(localization.logout, style: textStyle,),
            leading: Icon(Icons.exit_to_app),
            onTap: () {
              AuthenticationBloc authBloc = BlocProvider.of<AuthenticationBloc>(context);
              authBloc.dispatch(LoggedOut());
            },
          ),
          Divider(height: 1.0,)
        ],
      ),
    );
  }
}
