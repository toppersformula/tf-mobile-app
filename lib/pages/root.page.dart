import 'package:flutter/material.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/pages/account/account.page.dart';
import 'package:toppers_formula/pages/home/home.page.dart';
import 'package:toppers_formula/theme/theme.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _page = 0;
  PageController _pageController;
  @override
  void initState() {
    _pageController = PageController(initialPage: _page);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void onPageChanged(page) {
    if (!mounted) return;
    _pageController.jumpToPage(page);
    setState(() {
      _page = page;
    });
  }

  Future<bool> _onBackPressed() async {
    if (_page != 0) {
      _pageController.jumpTo(0);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
          body: PageView(
            physics: NeverScrollableScrollPhysics(),
            children: [HomePage(), Container(), AccountPage()],
            onPageChanged: onPageChanged,
            controller: _pageController,
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _page,
            backgroundColor: const Color.fromRGBO(248, 248, 248, 1.0),
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.list, color: _page == 0 ? primaryColor : iconGrey),
                  title: Text(localization.topics, style: TextStyle(color: _page == 0 ? primaryColor : iconGrey, fontWeight:  _page == 0 ? FontWeight.bold : FontWeight.w500)),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.library_books, color: _page == 1 ? primaryColor : iconGrey),
                  title: Text(localization.activities, style: TextStyle(color: _page == 1 ? primaryColor : iconGrey, fontWeight:  _page == 0 ? FontWeight.bold : FontWeight.w500)),
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person, color: _page == 2 ? primaryColor :  iconGrey),
                  title: Text(localization.account, style: TextStyle(color: _page == 2 ? primaryColor : iconGrey, fontWeight:  _page == 0 ? FontWeight.bold : FontWeight.w500)),
              ),
            ],
            onTap: onPageChanged,
          )),
    );
  }
}
