import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/login/login_bloc.dart';
import 'package:toppers_formula/logic/bloc/login/login_event.dart';
import 'package:toppers_formula/logic/bloc/login/login_state.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/utils/assets.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/text_field.dart';
import 'package:toppers_formula/widgets/shared/tf_alert_dialog.dart';

import '../../router.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  TextEditingController _emailController;
  LoginBloc loginBloc;

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _emailController?.dispose();
    loginBloc?.dispose();
  }

  Widget _emailWidget() => InputField(
      hintText: '',
      obscureText: false,
      hintStyle: hintStyle,
      contentPadding: EdgeInsets.symmetric(vertical: 10.0),
      textInputType: TextInputType.emailAddress,
      textStyle: textStyle,
      controller: _emailController,
      bottomMargin: 20.0,
      labelText: localization.email);

  Widget _submitButton(LoginState state) => Button(
        child: Text(
          localization.submit,
          style: textStyle.copyWith(color: Colors.white),
        ),
        backgroundColor: primaryColor,
        width: double.infinity,
        loading: state is LoginLoading,
        onPressed: () {
          loginBloc.dispatch(ResetPassword(
            email: _emailController.text,
          ));
        },
      );

  void listenStateChanges(context, state) {
    if (state is ResetPasswordFailure) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('${state.error}'),
          backgroundColor: Colors.red,
        ),
      );
    }

    if (state is ResetPasswordSuccess) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyAlertDialog(
              title: Text(
                localization.awesome,
                style:
                    TextStyle(fontSize: FONT_20, fontWeight: FontWeight.w600),
              ),
              content: Text(
                  localization.resetPassword),
              hasWarning: true,
              onConfirm: () {
                Navigator.pop(context);
                Navigator.pop(context);
              },
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
          appBar: TFAppBar(
            hasBackButton: true,
            title: localization.resetPassword,
          ),
          body: BlocListener<LoginBloc, LoginState>(
            listener: listenStateChanges,
            child:
                BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
              return SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(20.0),
                      child: Image.asset(
                        Assets.resetPassword,
                        fit: BoxFit.cover,
                        height: 180,
                        width: 180,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25.0),
                      child: Text(
                        localization.resetPasswordInfo,
                        style: TextStyle(fontSize: 15.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 40.0),
                    _emailWidget(),
                    SizedBox(height: 40.0),
                    _submitButton(state),
                  ],
                ),
              );
            }),
          )),
    );
  }
}
