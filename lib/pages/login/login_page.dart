import 'package:fluro/fluro.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/authentication/authentication.dart';
import 'package:toppers_formula/logic/bloc/login/login_bloc.dart';
import 'package:toppers_formula/logic/services/login_service.dart';
import 'package:toppers_formula/pages/login/reset_password.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/utils/assets.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/svg_icon.dart';

import '../../router.dart';
import 'login_form.dart';

class LoginPage extends StatelessWidget {
  Future<bool> onBackPress() {
    return Future<bool>.value(true);
  }

  Widget _forgotPasswordButton(BuildContext context) => Button(
    child: Text(
      "Forgot Password?",
      style: boldStyle,
    ),
    borderColor: primaryColor,
    isOutlined: true,
    width: double.infinity,
    borderWidth: 1.0,
    backgroundColor: Colors.white,
    onPressed: () async {
//      AppRouter.router.navigateTo(context, Routes.resetPassword);
      Navigator.of(context).push(MaterialPageRoute(
          fullscreenDialog: true,
          builder: (BuildContext ctx) => BlocProvider(
              builder: (context) => LoginBloc(
                  loginService: LoginService(),
                  authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)
              ),
              child: ResetPasswordPage()
          ),
      ));
    },
  );

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: onBackPress,
      child: SafeArea(
        top: false,
        bottom: false,
        child: Scaffold(
          appBar: TFAppBar(
            hasBackButton: true,
            title: localization.signIn,
          ),
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Center(
                  child: Container(
                    child: SvgIcon(
                      Assets.logoSvg,
                      size: 150.0,
                    ),
                  ),
                ),
                Text(localization.toppersFormula, style: Theme.of(context).textTheme.title.copyWith(color: primaryColor, fontWeight: FontWeight.bold),),
                SizedBox(height: 20.0,),
                Text(localization.welcome, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                SizedBox(height: 5.0,),
                Text(localization.enterEmail,),
                BlocProvider(
                  builder: (context) => LoginBloc(
                    loginService: LoginService(),
                    authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)
                  ),
                  child: LoginForm()
                ),
                SizedBox(
                  height: 15.0,
                ),
                _forgotPasswordButton(context),
                SizedBox(
                  height: 5.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: localization.byClickingHere,
                            style: textStyle.copyWith(
                                fontSize: FONT_SMALL_13, height: 1.5),
                            children: [
                              TextSpan(
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    AppRouter.router.navigateTo(context, Routes.termsOfUse, transition: TransitionType.inFromRight);
                                  },
                                  text: ' ${localization.termsOfUse}',
                                  style: textStyle.copyWith(
                                      color: primaryColor,
                                      fontSize: FONT_SMALL_13)),
                              TextSpan(
                                  text: ' ${localization.and}',
                                  style: textStyle.copyWith(
                                      color: Colors.black,
                                      fontSize: FONT_SMALL_13)),
                              TextSpan(
                                  recognizer: TapGestureRecognizer()..onTap = () {
                                    AppRouter.router.navigateTo(context, Routes.privacyPolicy, transition: TransitionType.inFromRight);
                                  },
                                  text: ' ${localization.privacyPolicy}',
                                  style: textStyle.copyWith(
                                      color: primaryColor, fontSize: FONT_SMALL_13))
                            ])),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
