import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/login/login_bloc.dart';
import 'package:toppers_formula/logic/bloc/login/login_event.dart';
import 'package:toppers_formula/logic/bloc/login/login_state.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/utils/assets.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/svg_icon.dart';
import 'package:toppers_formula/widgets/shared/text_field.dart';

import '../../router.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  TextEditingController _emailController;
  TextEditingController _passwordController;
  LoginBloc loginBloc;
  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _passwordController?.dispose();
    _emailController?.dispose();
    loginBloc?.dispose();
  }

  Widget _emailWidget() => InputField(
          hintText: '',
          obscureText: false,
          hintStyle: hintStyle,
          contentPadding: EdgeInsets.symmetric(vertical: 10.0),
          textInputType: TextInputType.emailAddress,
          textStyle: textStyle,
          controller: _emailController,
          bottomMargin: 20.0,
          labelText: localization.email,
        );


  Widget _passwordWidget() => InputField(
          hintText: '',
          obscureText: true,
          hintStyle: hintStyle,
          contentPadding: EdgeInsets.symmetric(vertical: 12.0),
          textInputType: TextInputType.text,
          textStyle: textStyle,
          controller: _passwordController,
          bottomMargin: 20.0,
          labelText: localization.password,
        );


  Widget _signInButton(LoginState state) => Button(
        child: Text(
          localization.signIn,
          style: textStyle.copyWith(color: Colors.white),
        ),
        backgroundColor: primaryColor,
        width: double.infinity,
        loading: state is LoginLoading,
        onPressed: () {
//          AppRouter.router.navigateTo(context, Routes.home, clearStack: true);
          loginBloc.dispatch(LoginButtonPressed(
            username: _emailController.text,
            password: _passwordController.text,
          ));
        },
      );


  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        }

        if (state is LoginInitial) {
          Navigator.pop(context);
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _emailWidget(),
            _passwordWidget(),
            SizedBox(
              height: 5.0,
            ),
            _signInButton(state),
          ],
        );
      }),
    );
  }
}
