import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/utils/assets.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/svg_icon.dart';

import '../../router.dart';

class AuthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.6), BlendMode.darken),
                image: AssetImage(Assets.loginBg),
              ),
            ),
          ),
          Align(
            child: Column(
              children: <Widget>[
                SizedBox(height: 80.0),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(100.0))),
                  child: SvgIcon(
                    Assets.logoSvg,
                    size: 150.0,
                  ),
                ),
                SizedBox(height: 10.0),
                Text(
                  localization.toppersFormula,
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .copyWith(color: Colors.white),
                ),
                SizedBox(
                  height: 100.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 22.0, vertical: 15.0),
                  child: TFButton(
                    childWidget: Text(
                      localization.signInWithEmail,
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    buttonColor: primaryColor,
                    onTap: () {
                      AppRouter.router.navigateTo(context, Routes.login);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 22.0, vertical: 15.0),
                  child: TFButton(
                    childWidget: Text(
                      localization.newHere,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w600),
                    ),
                    buttonColor: Colors.white,
                    onTap: () {
                      AppRouter.router.navigateTo(context, Routes.register);
                    },
                  ),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 40.0),
              child: GestureDetector(
                  onTap: () {
                    AppRouter.router.navigateTo(context, Routes.termsOfUse,
                        transition: TransitionType.inFromRight);
                  },
                  child: Text(
                    localization.termsOfUse,
                    style: TextStyle(
                        fontSize: FONT_MEDIUM,
                        color: Colors.white,
                        decoration: TextDecoration.underline),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
