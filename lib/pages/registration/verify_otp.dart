import 'package:flutter/material.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/utils/assets.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/svg_icon.dart';
import 'package:toppers_formula/widgets/shared/verification_code_input.dart';

class OTPVerificationPage extends StatefulWidget {
  @override
  _OTPVerificationPageState createState() => _OTPVerificationPageState();
}

class _OTPVerificationPageState extends State<OTPVerificationPage> {

  Widget _verifyButton() => TFButton(
    childWidget: Text("Verify", style: textStyle.copyWith(color: Colors.white),),
    buttonColor: primaryColor,
    onTap: () {},
  );

  Widget _resendButton() => TFOutlineButton(
    childWidget: Text("Resend", style: boldStyle,),
    borderColor: primaryColor,
    buttonColor: Colors.white,
    onTap: () async {
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TFAppBar(
        title: "Verification",
        hasBackButton: true,
      ),
      body: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SvgIcon(
                  Assets.verification,
                  size: 150.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35.0),
                  child: Text("Check your Email !! Provide us 6-digit verification code", style: TextStyle(fontSize: 14.0), textAlign: TextAlign.center,),
                ),
                SizedBox(height: 40.0),
                Text("Enter OTP Code", style: boldStyle,),
                SizedBox(height: 25.0),
                VerificationCodeInput(
                  length: 6,
                  itemSize: 55,
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  space: 20.0,
                  focusColor: primaryColor,
                  itemDecoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(3.0)),
                    border: Border.all(color: lightGrey100),
                  ),
                ),
                SizedBox(height: 40.0),
                _verifyButton(),
                SizedBox(height: 20.0),
                _resendButton()
              ],
            ),
          )
      ),
    );
  }
}
