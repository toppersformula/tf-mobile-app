import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/authentication/authentication_bloc.dart';
import 'package:toppers_formula/logic/bloc/register/register_bloc.dart';
import 'package:toppers_formula/logic/services/register_service.dart';
import 'package:toppers_formula/pages/registration/register_form.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';

class RegistrationPage extends StatelessWidget {
  Future<bool> onBackPress() {
    return Future<bool>.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: SafeArea(
          top: false,
          bottom: false,
          child: Scaffold(
              backgroundColor: Colors.white,
              appBar: TFAppBar(
                title: localization.createAccount,
                hasBackButton: true,
              ),
              body: SingleChildScrollView(
                padding: EdgeInsets.symmetric(
                    horizontal: 25.0, vertical: 20.0),
                child: BlocProvider<RegisterBloc>(
                    builder: (context) => RegisterBloc(
                        registerService: RegisterService(),
                        authenticationBloc:
                            BlocProvider.of<AuthenticationBloc>(context)),
                    child: RegisterForm()),
              )),
        ),
        onWillPop: onBackPress);
  }
}
