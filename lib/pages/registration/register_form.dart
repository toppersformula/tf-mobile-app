import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/logic/bloc/register/register_bloc.dart';
import 'package:toppers_formula/logic/bloc/register/register_event.dart';
import 'package:toppers_formula/logic/bloc/register/register_state.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/widgets/shared/button.dart';
import 'package:toppers_formula/widgets/shared/checkbox.dart';
import 'package:toppers_formula/widgets/shared/text_field.dart';
import 'package:toppers_formula/widgets/shared/tf_alert_dialog.dart';

import '../../router.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  TextEditingController _userNameController;
  TextEditingController _emailController;
  TextEditingController _phoneController;
  TextEditingController _passwordController;
  RegisterBloc registerBloc;

  @override
  void initState() {
    super.initState();
    _userNameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();
    _passwordController = TextEditingController();
    registerBloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _passwordController?.dispose();
    _userNameController?.dispose();
    _phoneController?.dispose();
    _emailController?.dispose();
    registerBloc?.dispose();
  }

  Future<bool> onBackPress() {
    return Future<bool>.value(true);
  }

  Widget _userNameWidget() => InputField(
        hintText: '',
        obscureText: false,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(vertical: 10.0),
        textInputType: TextInputType.text,
        textStyle: textStyle,
        controller: _userNameController,
        bottomMargin: 20.0,
        labelText: localization.fullName,
      );

  Widget _emailWidget() => InputField(
        hintText: '',
        obscureText: false,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(vertical: 10.0),
        textInputType: TextInputType.emailAddress,
        textStyle: textStyle,
        controller: _emailController,
        bottomMargin: 20.0,
        labelText: localization.emailAddress,
      );

  Widget _phoneWidget() => InputField(
        hintText: '',
        obscureText: false,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(vertical: 10.0),
        textInputType: TextInputType.number,
        textStyle: textStyle,
        controller: _phoneController,
        bottomMargin: 20.0,
        labelText: localization.phoneNumber,
      );

  Widget _passwordWidget() => InputField(
        hintText: '',
        obscureText: true,
        hintStyle: hintStyle,
        contentPadding: EdgeInsets.symmetric(vertical: 12.0),
        textInputType: TextInputType.text,
        textStyle: textStyle,
        controller: _passwordController,
        bottomMargin: 20.0,
        labelText: localization.password,
      );

  CheckBoxItem offerItem = CheckBoxItem(
      isSelected: false, title: localization.sendMeOffers);

  Widget _offersWidget() => Container(
        margin: EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            TFCheckBox(
              padding: 3.0,
              rounded: false,
              unSelectedColor: primaryColor,
              selectedColor: primaryColor,
              item: offerItem,
              callback: () {
                setState(() {
                  offerItem.isSelected = !offerItem.isSelected;
                });
              },
            ),
          ],
        ),
      );

  Widget _createAccountButton(RegisterState state) => Button(
        child: Text(
          localization.createAccount,
          style: textStyle.copyWith(color: Colors.white),
        ),
        backgroundColor: primaryColor,
        width: double.infinity,
        loading: state is RegisterLoading,
        onPressed: () {
//          AppRouter.router.navigateTo(context, Routes.verification);
          registerBloc.dispatch(RegisterButtonPressed(
              name: _userNameController.text,
              password: _passwordController.text,
              email: _emailController.text,
              phone: _phoneController.text));
        },
      );

  Widget _signInButton(RegisterState state) => Button(
        child: RichText(
            text: TextSpan(
                text: localization.haveAccount,
                style: textStyle,
                children: [
              TextSpan(
                  text: ' ${localization.signIn}',
                  style: textStyle.copyWith(color: primaryColor))
            ])),
        borderColor: primaryColor,
        backgroundColor: Colors.white,
        isOutlined: true,
        width: double.infinity,
        onPressed: () async {
          Navigator.of(context).pop();
          AppRouter.router.navigateTo(context, Routes.login);
        },
      );

  /// handle all the success and failure messages
  void _listenStateChange(context, state) {
    if (state is RegisterFailure) {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('${state.error}'),
          backgroundColor: Colors.red,
        ),
      );
    }

    if (state is RegisterInitial) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return MyAlertDialog(
              title: Text(
                localization.success,
                style:
                    TextStyle(fontSize: FONT_20, fontWeight: FontWeight.w600),
              ),
              content: Text(
                  localization.registrationSuccess),
              hasWarning: true,
              onConfirm: () {
                Navigator.pop(context);
                Navigator.pop(context);
                AppRouter.router.navigateTo(context, Routes.login);
              },
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: _listenStateChange,
      child:
          BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
        return Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                _userNameWidget(),
                _emailWidget(),
//                    _phoneWidget(),
                _passwordWidget(),
                _offersWidget(),
                SizedBox(height: 25.0),
                _createAccountButton(state),
                SizedBox(height: 20.0),
                _signInButton(state),
                SizedBox(
                  height: 50.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 40.0),
                  child: GestureDetector(
                    onTap: () {
                      AppRouter.router.navigateTo(context, Routes.termsOfUse,
                          transition: TransitionType.inFromRight);
                    },
                    child: Text(
                      localization.termsOfUse,
                      style: TextStyle(
                          fontSize: FONT_MEDIUM,
                          color: Colors.black,
                          decoration: TextDecoration.underline),
                    ),
                  ),
                ),
              ],
            ),
          ],
        );
      }),
    );
  }
}
