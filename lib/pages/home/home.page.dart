import 'package:flutter/material.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'package:toppers_formula/widgets/shared/appbar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool loadingCancel = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  // Build method for the Application
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: TFAppBar(
        title: localization.home,
        center: true
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text("Welcome to TF!", style: boldStyle.copyWith(fontSize: 25.0),),
          )
        ],
      ),
    );
  }
}
