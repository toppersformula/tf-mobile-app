/*
 * Created by Vijay Rathod<v.rathod@sap.com>
 */
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toppers_formula/localization/localization.dart';
import 'package:toppers_formula/pages/root.page.dart';
import 'logic/bloc/authentication/authentication.dart';
import 'pages/auth/auth.page.dart';
import 'pages/splash/splash_page.dart';
import 'utils/localstorage.dart';

class InitializationPage extends StatelessWidget {

  InitializationPage() {
    TFLocalStorage.setup();
  }

  @override
  Widget build(BuildContext pageContext) {
    Localization localization = Localization.of(pageContext);
    localization.setLocalization(pageContext);
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (pageContext, state) {
        if (state is AuthenticationAuthenticated) {
          return RootPage();
        }
        if (state is AuthenticationUnauthenticated) {
          return AuthPage();
        }

        if (state is AuthenticationLoading) {
          return CircularProgressIndicator();
        }
        return SplashPage();
      },
    );
  }
}
