/*
 * Created by Vijay Rathod<v.rathod@sap.com>
 */

class Assets {
  static const String logoSvg = "assets/images/logo.svg";
  static const String loginBg = "assets/images/login_bg.png";
  static const String verification = "assets/images/verification.svg";
  static const String resetPassword = "assets/images/reset_password.png";
}
