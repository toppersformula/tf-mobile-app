import 'package:shared_preferences/shared_preferences.dart';

const String USER_ID = 'UserId';
const String PHONE_NUMBER = 'PhoneNumber';
const TOKEN = 'Token';
const REFRESH_TOKEN = 'RefreshToken';
const FIRST_NAME = "FirstName";
const LAST_NAME = "LastName";
const FULL_NAME = "FullName";
const EMAIL = "Email";
const AVATAR_URL = "AvatarUrl";
const LOGGED_IN = "LoggedIn";

class TFLocalStorage {
  static SharedPreferences storage;

  static void setup() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    storage = await _prefs;
  }

  static void getStorage() => storage;

  static setItem(String key, dynamic value) async {
    if (storage == null) setup();

    if (value is int) await storage.setInt(key, value);

    if (value is String) await storage.setString(key, value);

    if (value is bool) await storage.setBool(key, value);
  }

  static getItem(String key) async {
    if (storage == null) setup();
    return await storage?.get(key);
  }

  static deleteItem(String key) async {
    if (storage == null) setup();
    await storage?.remove(key);
  }

  static clear() async {
    if (storage == null) setup();
    await storage?.clear();
  }
}
