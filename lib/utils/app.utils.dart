import 'package:flutter/material.dart';

class Utils {
  static Utils _instance;
  factory Utils() => _instance ??= new Utils._();
  Utils._();

  static showSnackbar(GlobalKey<ScaffoldState> scaffoldState, String message, {MaterialColor materialColor}) {
    if (message.isEmpty) return;
    // Find the Scaffold in the Widget tree and use it to show a SnackBar
    scaffoldState.currentState.showSnackBar(new SnackBar(content: new Text(message), backgroundColor: materialColor));
  }

  static showSnckbarWithScaffold(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(message)));
  }

  // Capitalize String
  static String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  static String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
}
