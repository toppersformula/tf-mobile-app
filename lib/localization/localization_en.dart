import 'localization.dart';

class LocalizationEN implements Localization {

  @override String get appName => "Topper's Formula";
  @override String get gettingReadyToGo => "Getting ready to go!";
  @override String get toppersFormula => "Topper's Formula";


  @override String get login => "Login";
  @override String get name => "Name";
  @override String get fullName => "Full Name";

  @override String get userName => "UserName";
  @override String get password => "Password";
  @override String get email => "Email";
  @override String get emailAddress => "Email Address";
  @override String get phoneNumber => "Phone Number";
  @override String get termsOfUse => "Terms of Use";
  @override String get signInWithEmail => "Sign in with Email";
  @override String get newHere => "New here? Create your account";
  @override String get success => "Success";

  /// tabs
  @override String get account => "Account";
  @override String get activities => "Activities";
  @override String get topics => "Topics";

  @override String get welcome => "Welcome!";
  @override String get enterEmail => "Enter email to sign in to your account";
  @override String get byClickingHere => 'By Clicking on "Sign in" you agree to our';
  @override String get privacyPolicy => "Privacy Policy";
  @override String get and => "and";
  @override String get signIn => "Sign In";

  /// reset password
  @override String get submit => "Submit";
  @override String get awesome => "Awesome!";
  @override String get resetPasswordSuccess => "Reset password link sent to your email. Please check your inbox.";
  @override String get resetPasswordInfo => "Submit";
  @override String get resetPassword => "Enter your email Adderss associated with your account and we will send you a link to reset your password";

  /// registration
  @override String get createAccount => "Create an Account";
  @override String get sendMeOffers => "Send me other offers and recommendations";
  @override String get haveAccount => "Have an account?";
  @override String get registrationSuccess => "Thanks for your registration to Topper's Formula. Please verify your email and login again.";

  /// home
  @override String get home => "Home";

  /// account
  @override String get logout => "Logout";


  @override setLocalization(context) {}
}