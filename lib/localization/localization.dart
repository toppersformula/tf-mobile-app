/*
 * Created by Vijay Rathod<v.rathod@sap.com>
 */
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:toppers_formula/localization/localization_in.dart';

import 'localization_en.dart';

Localization localization;
abstract class Localization {
  static Localization of(BuildContext context) {
    return Localizations.of<Localization>(context, Localization);
  }

  String get appName;
  String get gettingReadyToGo;
  String get toppersFormula;
  /// common
  String get name;
  String get userName;
  String get password;
  String get email;
  String get login;
  String get termsOfUse;
  String get signIn;
  String get emailAddress;
  String get fullName;
  String get phoneNumber;
  String get success;

  /// login
  String get signInWithEmail;
  String get newHere;
  String get welcome;
  String get enterEmail;
  String get byClickingHere;
  String get privacyPolicy;
  String get and;

  /// registration
  String get createAccount;
  String get sendMeOffers;
  String get haveAccount;
  String get registrationSuccess;
  /// reset password
  String get submit;
  String get awesome;
  String get resetPasswordSuccess;
  String get resetPasswordInfo;
  String get resetPassword;

  /// tabs
  String get topics;
  String get activities;
  String get account;

  /// home
  String get home;

  /// account
  String get logout;

  setLocalization(context) {
    localization = Localization.of(context);
  }
}

class TFLocalizationsDelegate extends LocalizationsDelegate<Localization> {
  const TFLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en'].contains(locale.languageCode);

  @override
  Future<Localization> load(Locale locale) => _load(locale);

  static Future<Localization> _load(Locale locale) async {
    final String name = (locale.countryCode == null || locale.countryCode.isEmpty) ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);
    Intl.defaultLocale = localeName;

    if (localeName?.toUpperCase() == "IN")
      return LocalizationIN();

    /// supports only english for now.
    return LocalizationEN();
  }

  @override
  bool shouldReload(LocalizationsDelegate<Localization> old) => false;
}