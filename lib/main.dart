import 'package:bloc/bloc.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app_init.dart';
import 'logic/bloc/authentication/authentication.dart';
import 'pages/auth/auth.page.dart';
import 'pages/home/home.page.dart';
import 'pages/splash/splash_page.dart';
import 'config/app_config.dart';
import 'localization/localization.dart';
import 'logic/bloc/authentication/authentication_bloc.dart';
import 'logic/bloc/authentication/authentication_event.dart';
import 'logic/bloc_helpers/simple_bloc_delegate.dart';
import 'logic/services/auth_service.dart';
import 'router.dart';
import 'theme/theme.dart';

void main() {
  AuthService authService = AuthService();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final Router router = Router();
  Routes.configureRoutes(router);
  AppRouter.router = router;
  print("MAIN_______");

  runApp(
    BlocProvider<AuthenticationBloc>(
        builder: (context) {
          return AuthenticationBloc(authService: authService)..dispatch(AppStarted());
        },
        child: ToppersFormulaApp()),
  );
}

class ToppersFormulaApp extends StatelessWidget {
  ToppersFormulaApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: InitializationPage(),
      title: APP_NAME,
      theme: appTheme,
      onGenerateRoute: AppRouter.router.generator,
      localizationsDelegates: [
        const TFLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [const Locale('en')],
      debugShowCheckedModeBanner: false,
    );
  }
}
