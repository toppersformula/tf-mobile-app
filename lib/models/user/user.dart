
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class User {
  String name;
  int phone;
  String email;
  int otp;
  String avatarUrl;

  User(this.name, this.email, this.phone, this.otp, this.avatarUrl);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}