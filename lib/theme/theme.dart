import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

const Color primaryColor = const Color.fromRGBO(58, 85, 159, 1);
const Color lightGrey = const Color.fromRGBO(186, 186, 186, 1);
const Color lightGrey100 = const Color.fromRGBO(223, 223, 223, 1);
const Color iconGrey = const Color.fromRGBO(168, 168, 168, 1);

ThemeData appTheme = new ThemeData(
  hintColor: lightGrey,
  primaryColor: primaryColor,
  fontFamily: 'proxima_nova',
  canvasColor: const Color(0xFFF8F8F8),
  textTheme: Typography(platform: defaultTargetPlatform).black.apply(
        bodyColor: Colors.blueGrey[900],
        displayColor: primaryColor,
      ),
);

// Padding
const double PADDING_TINY = 2.0;
const double PADDING_VERY_SMALL = 4.0;
const double PADDING_SMALL = 8.0;
const double PADDING_MEDIUM = 16.0;
const double PADDING_LARGE = 24.0;
const double PADDING_VERY_LARGE = 32.0;

// Fonts
const double FONT_VERY_SMALL = 4.0;
const double FONT_SMALL = 8.0;
const double FONT_MEDIUM = 16.0;
const double FONT_LARGE = 24.0;
const double FONT_VERY_LARGE = 32.0;
const double FONT_SMALL_14 = 14.0;
const double FONT_SMALL_12 = 12.0;
const double FONT_SMALL_13 = 13.0;
const double FONT_20 = 20.0;

//For Home Cards
const double FONT_SIZE_TITLE = 16.0;
const double FONT_SIZE_LABEL = 14.0;
const double FONT_SIZE_DATE = 12.0;

const double MARGIN_TINY = 2.0;
const double MARGIN_VERY_SMALL = 4.0;
const double MARGIN_SMALL = 8.0;
const double MARGIN_MEDIUM = 16.0;
const double MARGIN_LARGE = 24.0;
const double MARGIN_VERY_LARGE = 32.0;

TextStyle textStyle = const TextStyle(
    color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.normal);

TextStyle hintStyle = const TextStyle(
    color: lightGrey, fontSize: 16.0, fontWeight: FontWeight.normal);

TextStyle boldStyle = TextStyle(
    color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.w800);

TextStyle textStyleMute = const TextStyle(
    color: lightGrey, fontSize: 14.0, fontWeight: FontWeight.normal);

TextStyle appBarTextStyle = const TextStyle(
    color: Colors.black, fontWeight: FontWeight.w500, fontSize: 18.0);

TextStyle novaAltStyle = const TextStyle(
    color: Colors.white,
    fontSize: 16.0,
    fontFamily: "proxima_nova_alt",
    fontWeight: FontWeight.bold);

TextStyle buttonTextWithUnderlineStyle = const TextStyle(
    color: Colors.white,
    fontSize: 14.0,
    fontFamily: "lb",
    decoration: TextDecoration.underline,
    fontWeight: FontWeight.bold);

TextStyle boldBigWhiteStyle = const TextStyle(
  color: Colors.white,
  fontSize: 30.0,
  fontWeight: FontWeight.w900,
  shadows: [
    Shadow(color: Colors.black, offset: Offset(50.0, 50.0), blurRadius: 10.0)
  ],
);
