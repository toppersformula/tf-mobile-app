import 'package:flutter/material.dart';
import 'dart:math';

class MyCircularLoadingIndicator extends StatelessWidget {
  final Color innerColor;
  final Color outerColor;
  final double radius;
  const MyCircularLoadingIndicator({
    Key key,
    this.innerColor = Colors.white,
    this.outerColor = Colors.white,
    this.radius = 36.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SizedBox(
          height: radius,
          width: radius,
          child: CircularProgressIndicator(
            strokeWidth: 1.5,
            valueColor: AlwaysStoppedAnimation<Color>(innerColor),
          ),
        ),
        Transform(
          alignment: Alignment.center,
          transform: Matrix4.identity()
            ..scale(0.8)
            ..rotateZ(pi),
          child: SizedBox(
            height: radius,
            width: radius,
            child: CircularProgressIndicator(
              strokeWidth: 2.0,
              valueColor: AlwaysStoppedAnimation<Color>(outerColor),
            ),
          ),
        ),
      ],
    );
  }
}
