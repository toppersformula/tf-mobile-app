import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toppers_formula/theme/theme.dart';
import 'button.dart';

class MyAlertDialog extends StatelessWidget {
  final Widget title;
  final Widget content;
  final Function onConfirm;
  final Function onCancel;
  final bool hasActions;
  final bool hasWarning;
  const MyAlertDialog(
      {Key key,
        this.title,
        @required this.content,
        this.onConfirm,
        this.hasWarning = false,
        this.onCancel,
        this.hasActions = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      title: Container(
        alignment: Alignment.center,
        child: title ?? Container(),
      ),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          content,
          if (hasActions) SizedBox(height: 25.0),
          if (hasActions)
            Container(
              height: 45.0,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: hasWarning ?  Button(
                child: Text('Ok', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500),),
                textColor: Colors.white,
                // width: 120.0,
                width: 130.0,
                borderRadius: 23.0,
                height: 55.0,
                backgroundColor: primaryColor,
                onPressed: onConfirm,
              ) :  Row(
                // mainAxisAlignment: main,
                children: <Widget>[
                  Expanded(
                    child: Button(
                      child: Text('No'),
                      backgroundColor: Colors.transparent,
                      onPressed: onCancel,
                    ),
                  ),
                  SizedBox(width: 20.0),
                  // Spacer(),
                  Expanded(
                    child: Button(
                      child: Text('Yes'),
                      textColor: Colors.white,
                      // width: 120.0,
                      borderRadius: 23.0,
                      backgroundColor: primaryColor,
                      onPressed: onConfirm,
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
      // actions: actions,
      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15.0)),
    );
  }
}
