import 'package:flutter/material.dart';
import 'package:toppers_formula/theme/theme.dart';

import 'package:toppers_formula/widgets/shared/dot_spinner.dart';

class LBLoading extends StatelessWidget {
  final double size;
  LBLoading({this.size});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: size ?? 30.0,
        width: size ?? 30.0,
        child: CircularProgressIndicator(
          strokeWidth: 1.0,
          backgroundColor: primaryColor,
        ),
      ),
    );
  }
}

class LBDotLoading extends StatelessWidget {
  final Color color;
  LBDotLoading({this.color});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: DotSpinner(
        dotType: DotType.circle,
        dotOneColor: color ?? primaryColor,
        dotThreeColor: color ?? primaryColor,
        dotTwoColor: color ?? primaryColor,
      ),
    );
  }
}