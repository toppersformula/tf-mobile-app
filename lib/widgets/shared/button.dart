import 'package:flutter/material.dart';
import 'package:toppers_formula/widgets/shared/tf_circular_loading_indicator.dart';

class TFFlatButton extends StatelessWidget {
  final String buttonName;
  final VoidCallback onTap;

  final double height;
  final double width;
  final double bottomMargin;
  final double borderWidth;
  final Color buttonColor;

  final TextStyle textStyle = const TextStyle(
      color: const Color(0XFFFFFFFF),
      fontSize: 16.0,
      fontWeight: FontWeight.bold);

  //passing props in react style
  TFFlatButton(
      {this.buttonName,
        this.onTap,
        this.height,
        this.bottomMargin,
        this.borderWidth,
        this.width,
        this.buttonColor});

  @override
  Widget build(BuildContext context) {
    if (borderWidth != 0.0)
      return (new InkWell(
        onTap: onTap,
        child: new Container(
          width: width ?? 100.0,
          height: height ?? 50.0,
          margin: new EdgeInsets.only(bottom: bottomMargin ?? 2.0),
          alignment: FractionalOffset.center,
          decoration: new BoxDecoration(
              color: buttonColor,
              borderRadius: new BorderRadius.all(const Radius.circular(3.0)),
              border: new Border.all(
                  color: const Color.fromRGBO(221, 221, 221, 1.0),
                  width: borderWidth ?? 0.0)),
          child: new Text(buttonName, style: textStyle),
        ),
      ));
    else
      return (new InkWell(
        onTap: onTap,
        child: new Container(
          width: width ?? 100.0,
          height: height ?? 50.0,
          margin: new EdgeInsets.only(bottom: bottomMargin ?? 2.0),
          alignment: FractionalOffset.center,
          decoration: new BoxDecoration(
            color: buttonColor,
            borderRadius: new BorderRadius.all(const Radius.circular(30.0)),
          ),
          child: new Text(buttonName, style: textStyle),
        ),
      ));
  }
}

class TFButton extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final double height;
  final Color buttonColor;
  final double width;
  final Color textColor;
  final Widget childWidget;
  final bool enabled;
  final double elevation;
  final double radius;
  TFButton(
      {this.text,
        this.onTap,
        this.height,
        this.buttonColor,
        this.width,
        this.textColor,
        this.radius = 5.0,
        this.elevation = 1.0,
        this.childWidget,
        this.enabled = true});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: elevation,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(this.radius)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[childWidget],
      ),
      onPressed: onTap,
      minWidth: width,
      color: buttonColor.withOpacity(enabled ? 1 : 0.4),
      height: height ?? 50.0,
      textColor: textColor,
    );
  }
}

class TFOutlineButton extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final double height;
  final Color buttonColor;
  final double width;
  final Color textColor, borderColor;
  final Widget childWidget;
  final bool enabled;
  final double radius;

  TFOutlineButton(
      {this.text,
        this.onTap,
        this.height,
        this.buttonColor,
        this.width,
        this.radius = 4.0,
        this.textColor,
        this.borderColor,
        this.childWidget,
        this.enabled = true});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: Border.all(color: borderColor),
      onTap: onTap,
      child: Container(
        width: width ?? double.infinity,
        height: height ?? 50.0,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(color: borderColor),
          color: buttonColor
        ),
        padding: EdgeInsets.all(0.0),
        child: Center(
          child: childWidget,
        ),
      ),
    );
  }
}

class TFGradientButton extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final double height;
  final LinearGradient buttonGradient;
  final double width;
  final Color textColor, borderColor;
  final Widget childWidget;
  final bool enabled;
  final double radius;

  TFGradientButton(
      {this.text,
        this.onTap,
        this.height,
        this.buttonGradient,
        this.width,
        this.radius,
        this.textColor,
        this.borderColor,
        this.childWidget,
        this.enabled = true});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Opacity(
        opacity: enabled ? 1.0 : 0.4,
        child: Container(
          width: width ?? double.infinity,
          height: height ?? 50.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(2.0),
            gradient: buttonGradient,
          ),
          padding: EdgeInsets.all(0.0),
          child: Center(
            child: childWidget,
          ),
        ),
      ),
    );
  }
}

class Button extends StatefulWidget {
  ///whether the button is an outline button. Defaults to false.
  final bool isOutlined;

  ///Width of the button. Fills the width of its parent if not provided and the parent
  ///has definite width. If the parent has infinite width it defaults to 200.0.
  final double width;

  ///Height of the button. Fills the height of its parent if not provided and the parent
  ///has definite width. If the parent has infinite height it defaults to 50.0.
  final double height;

  ///child to be displayed in the button.
  final Widget child;

  ///Color of the text displayed in the button.
  final Color textColor;

  ///The text style that needs to be applied to the text. If not provided
  ///defaults to the default text style.
  final TextStyle textStyle;

  ///Background color of the button.
  final Color backgroundColor;

  ///Color of the button when it is disabled.
  final Color disabledColor;

  ///Color of the text button when it is disabled. Defaults to White
  final Color disabledTextColor;

  ///Gradient used as the background to the button. If provided, it overrides
  ///the backgroundColor property.
  final Gradient gradientBackground;

  ///Border radius of the button. Defaults to 6.5.
  final double borderRadius;

  ///The border color.
  final Color borderColor;

  ///Width of the border. Defaults to 1.5.
  final double borderWidth;

  ///Whether the button is in loading state. Defaults to false.
  final bool loading;

  final EdgeInsets padding;
  ///Callback that is executed when the button is pressed. If null the button
  ///will be disabled.
  @required
  final VoidCallback onPressed;

  const Button({
    Key key,
    @required this.child,
    this.backgroundColor = Colors.transparent,
    this.width = 50.0,
    this.height = 50.0,
    this.gradientBackground,
    @required this.onPressed,
    this.textStyle,
    this.textColor,
    this.borderRadius = 4.0,
    this.isOutlined = false,
    this.borderColor = Colors.white,
    this.borderWidth = 1.5,
    this.loading = false,
    this.disabledColor,
    this.padding,
    this.disabledTextColor = Colors.white,
  }) : super(key: key);

  @override
  _ButtonState createState() => _ButtonState();
}

class _ButtonState extends State<Button> with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _scaleAnimation;
  final int _animationDuration = 200;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        duration: Duration(milliseconds: _animationDuration), vsync: this);

    //Animation that shrinks down the button to provide the feedback of button being pressed.
    _scaleAnimation =
    Tween<double>(begin: 1.0, end: 0.98).animate(_animationController)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController.reverse();
        }
      });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
      BoxConstraints(minWidth: widget.width, minHeight: widget.height),
      child: GestureDetector(
        onTap: _handleTap,
        child: ScaleTransition(
          scale: _scaleAnimation,
          child: AnimatedContainer(
            padding: widget.padding,
            duration: Duration(milliseconds: _animationDuration),
            decoration: BoxDecoration(
              color: buttonColor(),
              gradient: widget.isOutlined || disabled || widget.loading
                  ? null
                  : widget.gradientBackground,
              border: widget.isOutlined
                  ? Border.all(
                color: disabled ? Colors.transparent : widget.borderColor,
                width: widget.borderWidth,
              )
                  : null,
              borderRadius:
              BorderRadius.all(Radius.circular(widget.borderRadius)),
            ),
            child: Center(
              widthFactor: 1.0,
              heightFactor: 1.0,
              child: !widget.loading
                  ? DefaultTextStyle(
                style: _textStyle(context),
                child: IconTheme(
                  data: IconThemeData(color: widget.textColor),
                  child: widget.child,
                ),
              )
                  : MyCircularLoadingIndicator(radius: 25.0,),
            ),
          ),
        ),
      ),
    );
  }

  bool get disabled => widget.onPressed == null;

  void _handleTap() {
    if (!disabled && !widget.loading) {
      _animationController.forward();
      widget.onPressed();
    }
  }

  TextStyle _textStyle(context) {
    TextStyle style = Theme.of(context).textTheme.body1;
    if (disabled) {
      return style.copyWith(color: widget.disabledTextColor);
    }
    return widget.textStyle ??
        style.copyWith(
          color: widget.textColor ?? Colors.black,
          fontSize: 14.0,
        );
  }

  Color buttonColor() {
    if (disabled) {
      if (widget.disabledColor != null) {
        return widget.disabledColor;
      }

      return Color.fromRGBO(198, 198, 198, 1.0);
    } else if (widget.loading) {
      if (widget.backgroundColor != null) {
        return widget.backgroundColor;
      }
    } {
      if (widget.isOutlined) {
        if (widget.backgroundColor != null) {
          return widget.backgroundColor;
        }
        return Colors.transparent;
      } else {
        return widget.backgroundColor;
      }
    }
  }
}
