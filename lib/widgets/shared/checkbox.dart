import 'package:flutter/material.dart';

class TFCheckBox extends StatelessWidget {
  final VoidCallback callback;
  final CheckBoxItem item;
  final double padding;
  final double stretchSpace;
  final TextStyle textStyle;
  final Color selectedColor;
  final Color unSelectedColor;
  final Color iconColor;
  final double iconSize;
  final bool rounded;

  TFCheckBox(
      {Key key,
        this.item,
        this.callback,
        this.padding,
        this.stretchSpace,
        this.selectedColor,
        this.iconColor,
        this.iconSize,
        this.textStyle,
        this.rounded = true,
        this.unSelectedColor});

  Widget _buildCheckBox(CheckBoxItem item, VoidCallback callback) =>
      GestureDetector(
        onTap: callback,
        child: Container(
          padding: EdgeInsets.all(padding ?? 5.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ClipRRect(
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(rounded ? 30 : 2),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(rounded ? 30 : 2),
                      color: item.isSelected ? selectedColor : Colors.transparent,
                      border: Border.all(
                          width: 1.0,
                          color: item.isSelected
                              ? Colors.transparent
                              : unSelectedColor)),
                  child: Container(
                    padding: EdgeInsets.all(padding ?? 3.0),
                    child: Icon(
                      Icons.check,
                      color: iconColor ?? Colors.white,
                      size: iconSize ?? 14.0,
                    ),
                  ),
                ),
              ),
              item.title != null && item.title != ""
                  ? SizedBox(
                width: stretchSpace ?? 10.0,
              )
                  : Container(),
              item.title != null && item.title != ""
                  ? Text(
                item.title,
                style: textStyle,
              )
                  : Container()
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return _buildCheckBox(item, callback);
  }
}

class CheckBoxItem {
  String title;
  bool isSelected;

  CheckBoxItem({this.title, this.isSelected});
}