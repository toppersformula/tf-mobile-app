import 'package:flutter/material.dart';
import 'package:toppers_formula/theme/theme.dart';

class TFAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool center;
  final bool hasBackButton;
  TFAppBar(
      {Key key,
      this.center = false,
      this.title = "",
      this.hasBackButton = false})
      : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(50.0);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: center,
      backgroundColor: primaryColor,
      title: Text(title),
      leading: hasBackButton
          ? IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          : null,
    );
  }
}
