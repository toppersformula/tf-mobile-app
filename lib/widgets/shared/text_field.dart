import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toppers_formula/theme/theme.dart';

class InputField extends StatelessWidget {
  final IconData icon;
  final String hintText;
  final TextInputType textInputType;
  final Color textFieldColor, iconColor;
  final bool obscureText;
  final double bottomMargin;
  final TextStyle textStyle, hintStyle, labelStyle;
  final validateFunction;
  final onChanged;
  final Key key;
  final String errorText;
  final int maxLength;
  final Widget suffixIcon;
  final String labelText;
  final TextEditingController controller;
  final List<TextInputFormatter> textInputFormatter;
  final TextAlign textAlign;
  final FocusNode focusNode;
  final bool autocorrect;
  final int maxLines;
  final double radius;
  final bool outline;
  final bool autofocus;
  final EdgeInsets contentPadding;
  final Color borderColor;
  //passing props in the Constructor.
  //Java like style
  InputField(
      {this.key,
      this.hintText,
      this.obscureText,
      this.textInputType,
      this.textFieldColor,
      this.controller,
      this.icon,
      this.iconColor,
      this.bottomMargin,
      this.textAlign,
      this.labelStyle,
      this.maxLines,
      this.radius,
      this.outline = false,
      this.textStyle,
      this.validateFunction,
      this.onChanged,
      this.maxLength,
      this.errorText,
      this.autocorrect,
      this.labelText,
      this.suffixIcon,
      this.contentPadding,
      this.focusNode,
      this.autofocus = false,
      this.textInputFormatter,
      this.borderColor = lightGrey100,
      this.hintStyle});

  @override
  Widget build(BuildContext context) {
    return (Container(
      margin:
          bottomMargin != null ? EdgeInsets.only(bottom: bottomMargin) : null,
      child: DecoratedBox(
        decoration: BoxDecoration(color: textFieldColor),
        child: TextFormField(
          style: textStyle,
          key: key,
          inputFormatters: textInputFormatter,
          obscureText: obscureText,
          keyboardType: textInputType,
          textAlign: textAlign ?? TextAlign.justify,
          maxLength: maxLength,
          onChanged: onChanged,
          maxLines: maxLines ?? null,
          autocorrect: autocorrect ?? false,
          controller: controller,
          autofocus: autofocus,
          focusNode: focusNode,
          decoration: InputDecoration(
            hintText: hintText,
            suffixIcon: suffixIcon ?? null,
            hintStyle: hintStyle,
            labelText: labelText,
            labelStyle: labelStyle,
            errorText: errorText,
            errorStyle: TextStyle(color: Colors.redAccent),
            contentPadding: contentPadding ?? EdgeInsets.only(top: 12.0),
            filled: false,
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: borderColor)),
            border: outline
                ? OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(radius ?? 50.0),
                    ),
                  )
                : UnderlineInputBorder(
                    borderSide: BorderSide(color: borderColor)),
            prefixIcon: icon != null
                ? Icon(
                    icon,
                    color: iconColor,
                  )
                : null,
          ),
        ),
      ),
    ));
  }
}
