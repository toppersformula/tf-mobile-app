import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  RegisterEvent([List props = const []]) : super(props);
}

class RegisterButtonPressed extends RegisterEvent {
  final String name;
  final String password;
  final String phone;
  final String email;

  RegisterButtonPressed({
    @required this.name,
    @required this.password,
    @required this.phone,
    @required this.email,

  }) : super([name, password, email, phone]);

  @override
  String toString() =>
      'RegisterButtonPressed { username: $name, password: $password, email: $email, phone: $phone }';
}