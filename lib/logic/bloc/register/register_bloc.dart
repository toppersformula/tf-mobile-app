import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:toppers_formula/logic/bloc/authentication/authentication.dart';
import 'package:toppers_formula/logic/services/register_service.dart';
import 'register_event.dart';
import 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final RegisterService registerService;
  final AuthenticationBloc authenticationBloc;
  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  RegisterBloc({
    @required this.registerService,
    @required this.authenticationBloc,
  })  : assert(registerService != null),
        assert(authenticationBloc != null);

  @override
  RegisterState get initialState => RegisterInitial();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterButtonPressed) {
      yield RegisterLoading();

      try {
        if (event.name == null || event.name == "") {
          yield RegisterFailure(error: "Please enter name.");
          return;
        }

//        if ( event.phone == null || event.phone == "")
//          yield RegisterFailure(error: "Please enter your phone number.");

        if (event.email == null || event.email == "") {
          yield RegisterFailure(error: "Please enter your email.");
          return;
        }

        if (event.password == null || event.password == "") {
          yield RegisterFailure(error: "Please enter a passowrd.");
          return;
        }

        if (event.password != null && event.password != "" && event.password.length < 6) {
          yield RegisterFailure(error: "Password must be at least 6 character");
          return;
        }

        if (!_isEmailValid(event.email)) {
          yield RegisterFailure(error: "Please enter valid email.");
          return;
        }

        await registerService.register(
          name: event.name,
          password: event.password,
          email: event.email,
          phone: event.phone
        );

        // authenticationBloc.dispatch(LoggedIn(token: token));

        yield RegisterInitial();
      } catch (error) {
        yield RegisterFailure(error: error.toString());
      }
    }
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }
}
