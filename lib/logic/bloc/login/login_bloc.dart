import 'dart:async';

import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:toppers_formula/logic/bloc/authentication/authentication.dart';
import 'package:toppers_formula/logic/services/login_service.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginService loginService;
  final AuthenticationBloc authenticationBloc;
  final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  LoginBloc({
    @required this.loginService,
    @required this.authenticationBloc,
  })  : assert(loginService != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
//        authenticationBloc.dispatch(LoggedIn(token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyYXRob2Qudmp5QGdtYWlsLmNvbSIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE1NjY3MjM0NjF9.mPk36IXiNya-_S_G5M-cVp1ubN6e_6bdYFU5gu0Ok7vEONRj_WLueJvyPhZvWFsGz8TQzE5H7GpAHPvFGgYL6w'));

//        yield LoginInitial();
//        return;
        if (event.username == null ||
            event.password == null ||
            event.password == "" ||
            event.username == "") {
          yield LoginFailure(error: "Please enter email and passowrd.");
          return;
        }

        if (!_isEmailValid(event.username)) {
          yield LoginFailure(error: "Please enter valid email.");
          return;
        }

        String idToken = await loginService.authenticate(
          username: event.username,
          password: event.password,
        );

        if (idToken != null && idToken != "") {
          authenticationBloc.dispatch(LoggedIn(token: idToken));
          yield LoginInitial();
        }
        yield LoginFailure(error: "You have entered wrong credentials. Please try again.");
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }

    if (event is ResetPassword) {
      try {
        if (event.email == null || event.email == "") {
          yield ResetPasswordFailure(error: "Please enter an email.");
          return;
        }

        if (!_isEmailValid(event.email)) {
          yield ResetPasswordFailure(error: "Please enter valid email.");
          return;
        }
        yield LoginLoading();

        await loginService.initResetPassword(email: event.email);
        yield ResetPasswordSuccess();
      } catch (error) {
        yield ResetPasswordFailure(error: error.toString());
      }
    }
  }

  bool _isEmailValid(String email) {
    return _emailRegExp.hasMatch(email);
  }
}
