import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginState extends Equatable {
  LoginState([List props = const[]]) : super(props);
}

class LoginSuccess extends LoginState {
  @override
  String toString() => 'LoginSuccess';
}

class LoginInitial extends LoginState {
  @override
  String toString() => 'LoginInitial';
}

class LoginLoading extends LoginState {
  @override
  String toString() => 'LoginLoading';
}

class LoginFailure extends LoginState {
  final String error;

  LoginFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'LoginFailure { error: $error }';
}

class ResetPasswordSuccess extends LoginState {
  @override
  String toString() => 'ResetPasswordSuccess';
}

class ResetPasswordFailure extends LoginState {
  final String error;

  ResetPasswordFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'ResetPasswordFailure { error: $error }';
}

