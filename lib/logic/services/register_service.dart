import 'package:toppers_formula/logic/restclient/endpoints.dart';
import 'package:toppers_formula/logic/restclient/rest_service_response.dart';
import 'package:toppers_formula/logic/restclient/restclient.dart';

import 'login_service.dart';

class RegisterService {
  Future<RestServiceResponse> register({
    String name, String phone, String email, String password
  }) async {
    String url = EndPoints.REGISTER;
    Map<String, dynamic> oData = Map();
    oData['fullName'] = name;
//    oData['phone'] = phone;
    oData['email'] = email;
    oData['password'] = password;
    print(oData.toString());
    RestServiceResponse response = await restClient.postAsync(url, oData, withoutToken: true);
    if (response.success) {
      /// if registration is successful then login user and navigate to home
//      LoginService loginService = LoginService();
//      String token = await loginService.authenticate();
//      if (token == null)
//        throw Exception('Authentication error');
//      return response;
    }
    if (!response.success)
      throw Exception('Something went wrong while registration process. Please try with another user');
    return null;
  }
}
