import 'package:toppers_formula/logic/restclient/endpoints.dart';
import 'package:toppers_formula/logic/restclient/rest_service_response.dart';
import 'package:toppers_formula/logic/restclient/restclient.dart';
import 'package:toppers_formula/utils/localstorage.dart';
import 'package:toppers_formula/utils/string_utils.dart';

class AuthService {
  Future<RestServiceResponse> getUserInfo({String username}) async {
    String url = StringUtils.format(EndPoints.USER_INFO, [username]);

    RestServiceResponse response = await restClient.getAsync(url);
    if (!response.success)
      throw Exception('Error while getting user information');
    return response;
  }

  Future<void> deleteToken() async {
    TFLocalStorage.deleteItem(TOKEN);
  }

  Future<void> persistToken(String token) async {
    TFLocalStorage.setItem(TOKEN, token);
  }

  Future<bool> hasToken() async {
    String token = await TFLocalStorage.getItem(TOKEN);
    return token != null && token != "";
  }
}