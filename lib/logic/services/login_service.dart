import 'package:toppers_formula/logic/restclient/endpoints.dart';
import 'package:toppers_formula/logic/restclient/rest_service_response.dart';
import 'package:toppers_formula/logic/restclient/restclient.dart';

class LoginService {

  Future<String> authenticate({String username, String password}) async {
    String url = EndPoints.AUTHENTICATE;
    Map<String, dynamic> oData = Map();
    oData['email'] = username;
    oData['password'] = password;

    RestServiceResponse response = await restClient.postAsync(url, oData, withoutToken: true);
    if (!response.success) {
      return null;
    }
    String token = response.content['id_token'];
    print("TOKEN: " + token);

    return token;
  }

  Future<RestServiceResponse> initResetPassword({String email}) async {
    String url = EndPoints.INIT_RESET_PASSWORD;

    RestServiceResponse response = await restClient.postAsync(url, email, withoutToken: true, hasPlainContentType: true);
    if (!response.success)
      throw Exception('Reset password init was not successful. Please try again.');
    return response;
  }
}
