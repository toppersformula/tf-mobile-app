class EndPoints {
  static const String AUTHENTICATE = 'authenticate';
  static const String REGISTER = 'register';
  static const String REFRESH_TOKEN = 'refresh_token';
  static const String ACTIVATE_ACCOUNT = 'activate';
  static const String INIT_RESET_PASSWORD = 'account/reset-password/init';
  static const String FINISH_RESET_PASSWORD = 'reset-password/finish';
  static const String CHANGE_PASSWORD = 'account/change-password';
  static const String USER_INFO = 'users/{0}';
  static const String DELETE_USER = 'users/{0}';
}