import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:toppers_formula/config/app_config.dart';
import 'package:toppers_formula/logic/restclient/rest_service_response.dart';
import 'package:toppers_formula/utils/localstorage.dart';

import 'endpoints.dart';

class RestClient {
  Map<String, String> headers = {
    "Content-Type": 'application/json',
    "Accept": 'application/json',
  };

  backendURL(endPoint) => BASE_URL + endPoint;

  Future<RestServiceResponse<T>> getAsync<T>(String resourcePath) async {
    String token = await TFLocalStorage.getItem(TOKEN);
    if (token == null) throw Exception("Token is invalid. Please contact administrator");
    headers['Authorization'] = "Bearer " + token;

    http.Response response = await http.get(backendURL(resourcePath), headers: headers);
    return processResponse<T>(response);
  }

  

  Future<RestServiceResponse<T>> postAsync<T>(String resourcePath, dynamic data, {bool withoutToken = false, bool hasPlainContentType = false}) async {
    if (!withoutToken) {
      String token = await TFLocalStorage.getItem(TOKEN);
      if (token == null) throw Exception("Token is invalid. Please contact administrator");
      headers['Authorization'] = "Bearer " + token;
      print(data.toString());
      print("POST ASYNC " + token.toString());
    }
    var content = hasPlainContentType ? data : json.encoder.convert(data);
    Map<String, String> customHeaders = Map();
    if (hasPlainContentType) {
      customHeaders['Content-Type'] = "text/plain";
    } else {
      customHeaders = headers;
    }
    http.Response response = await http.post(backendURL(resourcePath), body: content, headers: customHeaders);
    print(response.body);
    return processResponse<T>(response);
  }

  Future<RestServiceResponse<T>> getToken<T>(String resourcePath, dynamic data) async {
    print(backendURL(resourcePath));
    var content = json.encoder.convert(data);
    http.Response response = await http.post(backendURL(resourcePath), body: content, headers: headers);
    return processResponse<T>(response);
  }

  postRefreshToken<T>(String resourcePath, dynamic data) async {
    String token = await TFLocalStorage.getItem(TOKEN);
    if (token == null) throw Exception("Token is invalid. Please contact administrator");
    headers['Authorization'] = "Bearer " + token;
    var content = json.encoder.convert(data);
    http.Response response = await http.post(backendURL(resourcePath), body: content, headers: headers);
    print(response.body);
    var jsonResult = response.body;
    dynamic result = jsonDecode(jsonResult);
    String newToken = result['zabo-access-token'];
    print("new token: " + newToken);
    TFLocalStorage.setItem(TOKEN, newToken );
  }

  refreshToken() async {
    // Save token if  getting 401
    String refreshToken = await TFLocalStorage.getItem(REFRESH_TOKEN);
    var data = {"refresh_token": refreshToken};
    try {
      postRefreshToken(EndPoints.REFRESH_TOKEN, data);
    } catch (e) {
      print("Error while refreshing token..");
    }
  }

  RestServiceResponse<T> processResponse<T>(http.Response response) {
    print(response.statusCode);
    if (!((response.statusCode < 200) || (response.statusCode >= 300) || (response.body == null))) {
      var jsonResult = response.body;
      dynamic result = "";
      if (response.body != null && response.body != "")
      result = jsonDecode(jsonResult);

      return RestServiceResponse<T>(content: result, success: true);
    } else {
//      if (response.statusCode == 401) {
//        refreshToken();
//        print("Token refreshed successfully.......");
//      }
      dynamic errorResponse = response.body;
      return new RestServiceResponse<T>(
          success: false, content: errorResponse, message: "(${response.statusCode}) ${response.body != null || response.body != "" ? errorResponse.toString() : ""}");
    }
  }
}

RestClient restClient = new RestClient();